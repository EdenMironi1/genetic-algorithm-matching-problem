import random
import matplotlib.pyplot as plt

def parse_input(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    men_preferences = [list(map(int, line.split())) for line in lines[:30]]
    women_preferences = [list(map(int, line.split())) for line in lines[30:]]
    return men_preferences, women_preferences

def generate_initial_population(size, num_people):
    population = []
    for _ in range(size):
        matching = list(range(1, num_people + 1)) # Adjusted for 1-based indexing
        random.shuffle(matching)
        population.append(matching)
    return population

def fitness(matching, men_preferences, women_preferences):
    num_people = len(matching)
    inverse_matching = [0] * (num_people + 1) # Adjusted for 1-based indexing
    for i, w in enumerate(matching):
        inverse_matching[w] = i + 1 # Adjusted for 1-based indexing
    blocking_pairs = 0
    for m in range(1, num_people + 1):
        current_w = matching[m - 1]
        for w in men_preferences[m - 1]:
            if w == current_w:
                break
            if inverse_matching[w] <= num_people and women_preferences[w - 1].index(m) < women_preferences[w - 1].index(inverse_matching[w]):
                blocking_pairs += 1
                break
        for m_prime in women_preferences[current_w - 1]:
            if m_prime == m:
                break
            if men_preferences[m_prime - 1].index(current_w) < men_preferences[m_prime - 1].index(matching[m_prime - 1]):
                blocking_pairs += 1
                break
    return -blocking_pairs # Minimize blocking pairs, so fitness is negative of count

def crossover(parent1, parent2):
    size = len(parent1)
    crossover_point = random.randint(0, size - 1)
    child1 = parent1[:crossover_point] + [x for x in parent2 if x not in parent1[:crossover_point]]
    child2 = parent2[:crossover_point] + [x for x in parent1 if x not in parent2[:crossover_point]]
    return child1, child2

def mutate(matching):
    size = len(matching)
    i, j = random.sample(range(size), 2)
    matching[i], matching[j] = matching[j], matching[i]

def genetic_algorithm(file_path, population_size=100, generations=180, mutation_rate=0.3):
    men_preferences, women_preferences = parse_input(file_path)
    num_people = len(men_preferences)
    population = generate_initial_population(population_size, num_people)
    best_fitness = float('-inf')
    best_matching = None

    best_fitness_scores = []
    worst_fitness_scores = []
    average_fitness_scores = []

    for generation in range(generations):
        fitness_scores = [fitness(individual, men_preferences, women_preferences) for individual in population]
        population_with_fitness = sorted(zip(fitness_scores, population), key=lambda pair: pair[0], reverse=True)
        fitness_scores = [score for score, _ in population_with_fitness]
        population = [individual for _, individual in population_with_fitness]

        # Extract fitness metrics
        best_fitness_generation = fitness_scores[0]
        worst_fitness_generation = fitness_scores[-1]
        average_fitness_generation = sum(fitness_scores) / len(fitness_scores)

        best_fitness_scores.append(best_fitness_generation)
        worst_fitness_scores.append(worst_fitness_generation)
        average_fitness_scores.append(average_fitness_generation)

        if best_fitness_generation > best_fitness:
            best_fitness = best_fitness_generation
            best_matching = population[0]

        next_population = population[:population_size // 4]

        while len(next_population) < population_size:
            parents = random.sample(population[:population_size // 4], 2)
            child1, child2 = crossover(parents[0], parents[1])
            if random.random() < mutation_rate:
                mutate(child1)
            if random.random() < mutation_rate:
                mutate(child2)
            next_population.extend([child1, child2])

        population = next_population
        
    print("Best Matching:", best_matching)
    print("Fitness Score:", average_fitness_scores[-1], "as the best possible score is 0")
    
    # Plotting the fitness scores
    plt.figure(figsize=(12, 6))
    plt.plot(best_fitness_scores, label='Best Fitness')
    plt.plot(worst_fitness_scores, label='Worst Fitness')
    plt.plot(average_fitness_scores, label='Average Fitness')
    plt.xlabel('Generation')
    plt.ylabel('Fitness Score')
    plt.title('Fitness Scores Over Generations')
    plt.legend()
    plt.show()

# Usage
file_path = 'GA_input.txt'
genetic_algorithm(file_path)
